"""
Irenaeus Chan
12/13/2016

Phi and Psi Angle Calculator for entire Protein

Requires:
	- Complete Protein generated from protein.py
	- OR Complete Helix generated from helix.py
	- OR complete Sheet generated from sheet.py
"""

from itertools import tee, islice, chain, izip
import vector

def previousAndNext(some_iterable):
#http://stackoverflow.com/questions/1011938/python-previous-and-next-values-inside-a-loop
	prevs, items, nexts = tee(some_iterable, 3)
	prevs = chain([None], prevs)
	nexts = chain(islice(nexts, 1, None), [None])
	return izip(prevs, items, nexts)

#The calculator that takes in the protein and determines the Phi and Psi angles
def calculatePhiPsi(protein):
	with open('ramachandranplots.csv', 'a') as output:
		for prev, aminoacid, nxt in previousAndNext(protein.aminoAcidList):
			"""
			Due to how Phi and Psi angles are calculated we can't calculate the beginning and the end of the sequence
			as there is no previous points to help generate the initial Phi angle. However, even though can calculate 
			the first Psi angle, the number is quite useless for a Ramachandran plot without its corresponding Phi

			Therefore, the calculation for the first residue will be skipped

			We also need to take into account when the seqres changes, proteins are generally made up of a series of
			residue chains. Therefore, we need to make sure we stop calculating at the end of one residue chain, and
			start a new series of calculations for the next residue chain
			"""
			if(prev is None or prev.seqres != aminoacid.seqres):
				"""
				With the first residue being skipped for every residue chain, we still need some information from the
				first residue, primarily the C atom. Which is the 3rd index of the backbone in the Amino Acid Object
				"""
				C = [aminoacid.backboneAtoms[2].x, aminoacid.backboneAtoms[2].y, aminoacid.backboneAtoms[2].z]
				continue
			elif(nxt is None or aminoacid.seqres != nxt.seqres): continue
			"""
			We need this for the end of the entire protein and the ending of the residue chain. Similar to the starting
			of the residue chain, there won't be another residue to follow either case, so we won't be able to calculate
			the Psi angle for the last residue, so we will skip the last residue calculation
			"""

			#Initialize the other 2 components of the dihedral angle
			N = [aminoacid.backboneAtoms[0].x, aminoacid.backboneAtoms[0].y, aminoacid.backboneAtoms[0].z]
			Ca = [aminoacid.backboneAtoms[1].x, aminoacid.backboneAtoms[1].y, aminoacid.backboneAtoms[1].z]

			"""
			The Phi and Psi angles are essentially dihedral angles.
			Phi is the angle between the plane joining the previous C, current N, and current alpha-C and the plane 
			joining the current N, the current alpha-C and, the current C
			Psi is the angle between the plane joining the current N, the current alpha-C, and the current C and the
			plane joining the current alpha-C, current C, and the next N

			One can find the dihedral angle of the two planes by finding the normal vector to the two planes in question
			and then calculating the angles between the two planes in question. This is the method I am using.
			"""
			
			#------------------------------------------------------PHI------------------------------------------------------
			vectorCN = vector.vectorCalculation(C, N)					#Prev C, with current N
			vectorNCa = vector.vectorCalculation(N, Ca)					#Current N with current alpha-C
			normalVector1 = vector.crossProduct(vectorCN, vectorNCa)	#Determine the normal vector to the pC, cN, and cCa

			"""
			Once we've defined the first plane of the Phi angle, we move on to the second plane. The only thing that 
			changes from the first plane to the second plane is the prev C is now going to be the current C
			"""
			C = [aminoacid.backboneAtoms[2].x, aminoacid.backboneAtoms[2].y, aminoacid.backboneAtoms[2].z]
			vectorCaC = vector.vectorCalculation(Ca, C)					#Find the new vector
			normalVector2 = vector.crossProduct(vectorNCa, vectorCaC)	#Determine the other normal vector to cN, cCa, and cC

			
			"""
			The cross product vectors are both normal to the axis vectorNCa (central vector),so the angle between them is
			the dihedral angle that we are looking for. However... since the "angle" only returns values between 0 and pi,
			we need to make sure we get the right sign relative to the rotation axis
			"""
			phi = vector.dihedralAngle(normalVector1, normalVector2)
			if vector.dotProduct(vector.crossProduct(normalVector1, normalVector2), vectorNCa) < 0: phi = -phi

			#------------------------------------------------------PSI------------------------------------------------------
			normalVector1 = vector.crossProduct(vectorNCa,vectorCaC)	#We treat the previous normalVector2 as the new normalVector1
			N = [nxt.backboneAtoms[0].x, nxt.backboneAtoms[0].y, nxt.backboneAtoms[0].z]
																		#Similar to what we did in the Phi calculations, Psi is next N
			vectorCN = vector.vectorCalculation(C, N)					#From our new nitrogen coordinate, we can calculate the vector
			normalVector2 = vector.crossProduct(vectorCaC, vectorCN)	#Find the other normal vector

			psi = vector.dihedralAngle(normalVector1, normalVector2)	#Same calculation we used for phi
			if vector.dotProduct(vector.crossProduct(normalVector1, normalVector2), vectorCaC) < 0: psi = -psi

			output.write(aminoacid.aminoAcid + "," + str(phi) + "," + str(psi) + "\n")