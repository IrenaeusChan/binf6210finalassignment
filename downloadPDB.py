import sys
import urllib

if __name__ == '__main__':
	pdbname = []
	with open(sys.argv[1], "r") as stream:
		for line in stream:
			pdbname.append(line.strip())

	for name in pdbname:
		url = 'http://www.rcsb.org/pdb/files/{0}.pdb'.format(name)
		with open('{0}.pdb'.format(name), 'w') as output: output.write(urllib.urlopen(url).read())