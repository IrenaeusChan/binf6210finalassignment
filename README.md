# README #

BINF 6210 Final Assignment - Developed in Python 2.7.12

Clone the repository into your working directory. In order to run the program simply change repositories to the one containing this repository and type:

python main.py -help

This will bring you to a list of possible options for the program to run.

The options are: python main.py 

* PDB File: To calculate the Torsion Angles for a single PDB File
* -a Directory: To calculate the Torsion Angles for an entire directory
* -h -a Directory: To calculate the Torsion Angles for ONLY Helices in an entire Directory
* -s -a Directory: To calculate the Torsion Angles for ONLY Sheets in an entire Directory
* -d PDBFile: To download and load your own PDB File from the Protein Data Bank

### Description ###

For the Final Assignment of the BINF6210 Course, I will examine the [Ramachandran Plots](https://en.wikipedia.org/wiki/Ramachandran_plot) as described by Ramachandran et al. 1963. The python scrypt that is found in this repository downloads or reads existing PDB Files from the [Protein Data Bank Website](http://www.rcsb.org/pdb/home/home.do) and parses through the information to calculate the Phi and Psi Dihedral Angles. The program can also be specific towards only calculating this information for only helices and only sheets.

### Files ###

| Filename              | Description                         |
| --------------------- | ----------------------------------- |
| main.py               | The main program                    |
| downloadPDB.py        | The program used to download all PDB Files|
| Library\atom.py       | Atom Class                          |
| Library\aminoacid.py  | Amino Acid Class --> Atom Object    |
| Library\protein.py    | Protein Class --> Amino Acid Object |
| Library\helix.py      | Subset of Proteins with only Helix  |
| Library\sheet.py      | Subset of Proteins with only Sheets |
| Library\coil.py       | Subset of Proteins with only Coils  |
| Library\vector.py     | Mathmematical Vector Library        |
| Library\phipsi.py     | Dihedral Angle Calculations Library |
| TrainingSet           | Directory containing 17 test files  |
| TestingSet            | The PDB Files used to generate the Ramachandran Plots|
| Ramachandran Plots    | Contains the CSV Files and Generated Plots (.png)|
| ramachandranplotter.R | The R script used to generate the Ramachandran Plots|

### Limitations and Assumptions ###

To use this program, everything must be used using the provided Library. The program is not meant to be extendable to other libraries and is not fully fleshed out in terms of error prevention. I have tried to make the program as user friendly as possible, but there are still many cases where the program may crash. 

Additionally, the program assumes proper PDB File formatting. If a PDB File is improperly formatted, there is no defense against File Input. Certain common "bad" inputs are the presence of heavy metals in the PDB File, newer PDB Files using Hydrogens, and missing information from incomplete data sets.

Program will output all Dihedral Angles to a CSV File and cannot be changed.

Program gives very little control to the user due to the program being mainly used for research and testing purposes.

### Author ###

Irenaeus Chan -- irenaeus@mail.uoguelph.ca