#Importing libraries from Python
import sys
import string
import glob
import os
import urllib
#Importing the Classes from my own Library
sys.path.append(os.path.realpath("Library"))
from phipsi import calculatePhiPsi
import protein
import sheet
import helix
import coil

PATH = os.getcwd()											#The current director the user is working from

def formatFilename(filename):
	drive, pathAndFile = os.path.splitdrive(filename)		#http://stackoverflow.com/questions/3167154/how-to-split-a-dos-path-into-its-components-in-python
	filePath, fileformat = os.path.split(pathAndFile)
	return fileformat

#A method for computing all the PDB Files in a given directory
def computeAll():
	count = 0
	for filename in glob.glob(os.path.join(PATH, '*.pdb')):
		count+=1
		print "{0} files completed...{1}".format(count, formatFilename(filename))
		thisProtein = protein.buildProtein(filename)
		calculatePhiPsi(thisProtein)

def computeAllHelix():
	count = 0
	for filename in glob.glob(os.path.join(PATH, '*.pdb')):
		count+=1
		print "{0} files completed...{1}".format(count, formatFilename(filename))
		thisProtein = protein.buildProtein(filename)
		helixList = helix.buildHelix(filename, thisProtein)
		if (len(helixList)<1): print "There were no HELICES found in {0}".format(formatFilename(filename))
		else: 
			for thisHelix in helixList:
				calculatePhiPsi(thisHelix)

def computeAllSheet():
	count = 0
	for filename in glob.glob(os.path.join(PATH, '*.pdb')):
		count+=1
		print "{0} files completed...{1}".format(count, formatFilename(filename))
		thisProtein = protein.buildProtein(filename)
		sheetList = sheet.buildSheet(filename, thisProtein)
		if (len(sheetList)<1): print "There were no SHEETS found in {0}".format(formatFilename(filename))
		else: 
			for thisSheet in sheetList:
				for thisStrand in thisSheet.strandList:
					calculatePhiPsi(thisStrand)

def computeAllCoil():
	count = 0
	for filename in glob.glob(os.path.join(PATH, '*.pdb')):
		count+=1
		print "{0} files completed...{1}".format(count, formatFilename(filename))
		thisProtein = protein.buildProtein(filename)
		coilList = coil.buildCoil(filename, thisProtein)
		if (len(coilList)<1): print "There were no COILS found in {0}".format(formatFilename(filename))
		else: 
			for thisCoil in coilList:
				calculatePhiPsi(thisCoil)

def downloadPDBFile(pdbname):
	url = 'http://www.rcsb.org/pdb/files/{0}.pdb'.format(pdbname)
	with open('{0}.pdb'.format(pdbname), 'w') as output: output.write(urllib.urlopen(url).read())

if __name__ == '__main__':
	if len(sys.argv) < 2: print "\nERROR: No file was provided\nFORMAT:python main.py filename.pdb\npython main.py -help for more options"
	elif os.path.isfile(sys.argv[1]) == True and sys.argv[1].endswith(".pdb"):	#Calculates Dihedral Angles for a single File
		print "\nDetermining Ramachandran Plot Coordinates using File: {0}".format(sys.argv[1])
		thisProtein = protein.buildProtein(sys.argv[1])
		calculatePhiPsi(thisProtein)
	elif (len(sys.argv) > 2 and sys.argv[1] == "-a"):
		PATH += sys.argv[2]
		if (os.path.isdir(PATH) == True):
			print "\nComputing ALL PDB Files found in the Directory: {0}".format(sys.argv[2])
			computeAll()
		else: print "\nERROR: This directory does not exist, please make sure format is correct\nFORMAT: python main.py -a \Directory"
	elif (len(sys.argv) > 3 and sys.argv[1] == "-h" and sys.argv[2] == "-a"):
		PATH += sys.argv[3]
		if (os.path.isdir(PATH) == True):
			print "\nComputing ALL PDB Files found in the Directory: {0}".format(sys.argv[3])
			computeAllHelix()
		else: print "\nERROR: This directory does not exist, please make sure format is correct\nFORMAT: python main.py -h -a \Directory"
	elif (len(sys.argv) > 3 and sys.argv[1] == "-s" and sys.argv[2] == "-a"):
		PATH += sys.argv[3]
		if (os.path.isdir(PATH) == True):
			print "\nComputing ALL PDB Files found in the Directory: {0}".format(sys.argv[3])
			computeAllSheet()
		else: print "\nERROR: This directory does not exist, please make sure format is correct\nFORMAT: python main.py -s -a \Directory"
	elif (len(sys.argv) > 3 and sys.argv[1] == "-c" and sys.argv[2] == "-a"):
		PATH += sys.argv[3]
		if (os.path.isdir(PATH) == True):
			print "\nComputing ALL PDB Files found in the Directory: {0}".format(sys.argv[3])
			computeAllCoil()
		else: print "\nERROR: This directory does not exist, please make sure format is correct\nFORMAT: python main.py -c -a \Directory"
	elif (len(sys.argv) > 2 and sys.argv[1] == "-d"):
		downloadPDBFile(sys.argv[2])
	elif (sys.argv[1] == "-help"):
		print "\nOptions Available:"
		print "\n\t-a\t\t-To compute all PDB Files in a Directory"
		print "\t-h -a\t\t-To compute all Helices from PDB Files in a Directory"
		print "\t-s -a\t\t-To compute all Sheets from PDB Files in a Directory"
		print "\t-c -a\t\t-To compute all Coils from PDB Files in a Directory **Not Working"
		print "\t-d PDBFile\t-To download an existing PDB File and save it to this directory"
		print "\nTo run the code:"
		print "python main.py [Flag] [File/Directory]"
	else:
		print "FORMAT:python main.py filename.pdb\npython main.py -help for more options"

